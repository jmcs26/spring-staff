package com.service;

import com.ciit.training.staff.model.Employee;
import com.ciit.training.staff.service.EmployeeService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


 
@SpringBootTest
public class EmployeeServiceTests {

    @Autowired
    private EmployeeService employeeService;

    @Test
    public void test_save_sanityCheck(){
        Employee testEmployee = new Employee();
        testEmployee.setName("Mr Test Data");
        testEmployee.setAddress("133 Test Address");

        employeeService.save(testEmployee);
    }


    @Test
    public void test_findAll_sanityCheck(){
        assert(employeeService.findAll().size() == 0);
    }
    
}
