package com.ciit.training.staff.dao;

import com.ciit.training.staff.model.Employee;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface EmployeeMongoRepo extends MongoRepository<Employee,String> {

    
    
}
